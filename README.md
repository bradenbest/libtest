# libtest

Minimalistic ANSI C unit testing library

## Usage

`example.c` is a great way to quickly see how this works.

In brief:

* `#include "unittest.h"`
* write your test functions (type: `int (void)`)
* make a `struct test_node[]` object, give each entry a function pointer and a description
* call `run_tests` with the array and its length
* compile your test module with `unittest.c`

It's that simple. `run_tests()` will handle the rest. It will additionally return the number of passed tests, so if you
want to return that from `main` so an external shell script can use it, you can do that as well. Note that if said test,
where main returns the number of tests passed, is run in a makefile, make will erroneously report that the build "failed",
because that's just what happens when a program in a make pipeline returns anything but zero.

Since run tests takes a pointer and a length, it is also possible to run only a subset of the tests

    run_tests(tests, 7);     // run all 7 tests
    run_tests(tests, 3);     // run only the first 3 tests
    run_tests(tests + 3, 4); // run only the last 4 tests
