#include <stdio.h>

#include "unittest.h"

/* Define functions to test. One is implemented incorrectly. */
int
square(int x)
{
    return x * x;
}

int
square_wrong(int x)
{
    return x / 2;
}

/* Demonstrate TESTFN macro */
TESTFN(test_square_1, square(0) == 0)
TESTFN(test_square_2, square(4) == 16)
TESTFN(test_square_3, square(-4) == 16)
TESTFN(test_square_wrong_1, square_wrong(0) == 0)
TESTFN(test_square_wrong_2, square_wrong(4) == 16)
TESTFN(test_fail_arbitrarily, FAIL)
TESTFN(test_pass_arbitrarily, PASS)

/* Demonstrate test that does more than return an expression */
int
test_nontrivial(void)
{
    int numbers[6] = { 0, 1 };

    for(int i = 0; i < 4; ++i)
        numbers[i + 2] = numbers[i] + numbers[i + 1];

    for(int i = 0; i < 6; ++i)
        printf("%i ", numbers[i]);

    putchar('\n');
    return numbers[5] == 5;
}

int
main(void)
{
    /* Define a (struct test_node[]) with test functions and strings describing what they're testing */
    struct test_node tests[] = {
        /* 1 */ { test_square_1, "square(0) == 0?" },
        /* 2 */ { test_square_2, "square(4) == 16?" },
        /* 3 */ { test_square_3, "square(-4) == 16?" },
        /* 4 */ { test_square_wrong_1, "square_wrong(0) == 0?" },
        /* 5 */ { test_square_wrong_2, "square_wrong(4) == 16?" },
        /* 6 */ { test_fail_arbitrarily, "guaranteed to fail" },
        /* 7 */ { test_pass_arbitrarily, "guaranteed to pass" },
        /* 8 */ { test_nontrivial, "6th fibonacci is 5?" },
    };

    /* call run_tests with the above array and its length */
    return run_tests(tests, 8);
}
