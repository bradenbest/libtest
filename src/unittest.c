#include <stdio.h>

#include "unittest.h"

static char const * passfail_str[2] = { "FAIL", "PASS" };

int
run_tests(struct test_node *tests, size_t ntests)
{
    int total_passed = 0;
    int current_result;

    for(size_t i = 0; i < ntests; ++i){
        current_result = !!(tests[i].fn());
        total_passed += current_result;
        printf("[%s] %3lu of %lu: %s\n", passfail_str[current_result], i + 1, ntests, tests[i].desc);
    }

    printf("%i of %lu tests passed.\n", total_passed, ntests);
    return total_passed;
}
