#ifndef UNITTEST_H
#define UNITTEST_H

/* size_t def */
#include <stddef.h>

/* for simple unit tests that only test a single expression. */
#define TESTFN(name, expr) \
    int name(void){ return (expr); }

/* must return 0 or 1 for FAIL/PASS */
typedef int (*testfn_t)(void);

enum test_passfail {
    FAIL,
    PASS
};

struct test_node {
    testfn_t  fn;
    char *    desc;
};

int run_tests(struct test_node *tests, size_t ntests);

#endif
